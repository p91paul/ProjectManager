import QtQuick 2.2
import QtQuick.Controls 1.4

Item {
    id: item1
    TableView {
        anchors.top: parent.top
        anchors.topMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.right: parent.right
        anchors.rightMargin: 0
        height: 300
        TableViewColumn {
            id: projectColumn
            title: "Project"
            role: "ProjectId"
            movable: false
            resizable: false
            width: 100
        }
        TableViewColumn {
            id: taskDescriptionColumn
            title: "Description"
            role: "TaskDescription"
            movable: false
            resizable: false
            width: 100
        }

        model: tasksModel
    }
}
