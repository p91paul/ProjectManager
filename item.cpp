/*
This file is part of ProjectManager.

ProjectManager is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ProjectManager is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ProjectManager.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "item.h"


//Item
Item::Item(QSqlTableModel* model, int row) : super(model, row) {}

//ItemModel
ItemModel::ItemModel(QObject *parent, QSqlDatabase db)
    : QMLTableModel<Item>(parent, db) {}

Item* ItemModel::getRow(int i) { return _getRow(i); }
int ItemModel::addRow() { return _addRow(); }
void ItemModel::removeRow(int i) { _removeRow(i); }

int ItemModel::containerId() const {
    return m_containerId;
}

QString ItemModel::containerType() const
{
    return m_containerType;
}

void ItemModel::setContainerId(int containerId) {
    if (m_containerId == containerId)
        return;

    m_containerId = containerId;
    auto filter = QString("ContainerType = '%1' AND ContainerId = %2")
            .arg(m_containerType).arg(containerId);
    //qDebug() << "id: " << containerId << " filter: " << filter;
    this->setFilter(filter);
    this->select();
    emit containerIdChanged(m_containerId);
}

void ItemModel::setContainerType(QString containerType)
{
    if (m_containerType == containerType)
        return;

    m_containerType = containerType;
    emit containerTypeChanged(m_containerType);
}
