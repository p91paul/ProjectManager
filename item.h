/*
This file is part of ProjectManager.

ProjectManager is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ProjectManager is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ProjectManager.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef ITEM_H
#define ITEM_H

#include "modelrowdto.h"

class Item : public ModelRowDto {
    typedef ModelRowDto super;
    Q_OBJECT
public:
    explicit Item(QSqlTableModel* model, int row);

    DTO_COLUMN(ItemID)
    DTO_COLUMN(ContainerId)
    DTO_COLUMN(ContainerType)
    DTO_COLUMN(ItemType)
    DTO_COLUMN(ItemContent)
    DTO_COLUMN(ItemOrder)
};

class ItemModel : public QMLTableModel<Item> {
    Q_OBJECT
    Q_PROPERTY(int containerId READ containerId WRITE setContainerId NOTIFY containerIdChanged)
    Q_PROPERTY(QString containerType READ containerType WRITE setContainerType NOTIFY containerTypeChanged)
public:
    ItemModel(QObject *parent = Q_NULLPTR, QSqlDatabase db = QSqlDatabase());

    Q_INVOKABLE virtual Item* getRow(int i);
    Q_INVOKABLE int addRow();
    Q_INVOKABLE void removeRow(int i);
    int containerId() const;
    QString containerType() const;

public slots:
    void setContainerId(int containerId);
    void setContainerType(QString containerType);

signals:
    void containerIdChanged(int containerId);
    void containerTypeChanged(QString containerType);

private:
    int m_containerId;
    QString m_containerType;
};

#endif // ITEM_H
