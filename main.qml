/*
This file is part of ProjectManager.

ProjectManager is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ProjectManager is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ProjectManager.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.2
import QtQuick.Window 2.2
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Controls 2.2
import QtQuick.Dialogs 1.2

Window {
    id: window
    visible: true
    title: qsTr("Project Manager")
    visibility: Window.Maximized
    Component.onCompleted: {
        if (sqlError)
            sqlErrorDialog.open();
    }

    TabBar {
        id: mainTabBar
        width: parent.width
        currentIndex: 0
        TabButton {
            text: qsTr("Projects")
        }
        /*TabButton {
            text: qsTr("Tasks")
        }*/
    }

    StackLayout {
        anchors.top: mainTabBar.bottom
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.topMargin: 5
        currentIndex: mainTabBar.currentIndex

        ProjectView {
            id: projectsTab
            anchors.fill: parent
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignLeft | Qt.AlignTop
        }

        /*TaskView {
            id: taskTab
            anchors.fill: parent
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignLeft | Qt.AlignTop
        }*/
    }

    MessageDialog {
        id: sqlErrorDialog
        title: "SQLError"
        text: sqlError
    }
}


