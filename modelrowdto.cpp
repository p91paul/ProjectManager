/*
This file is part of ProjectManager.

ProjectManager is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ProjectManager is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ProjectManager.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "modelrowdto.h"


ModelRowDto::ModelRowDto(QSqlTableModel *model, int row):
    model(model), row(row) {}

QVariant ModelRowDto::getProperty(QString name) {
    return getIndex(name).data();
}

QModelIndex ModelRowDto::getIndex(QString name) {
    return model->index(row, model->fieldIndex(name));
}

bool ModelRowDto::setProperty(QString name, QVariant value) {
    if (value == getProperty(name))
        return false;

    model->setData(getIndex(name), value);
    return true;
}
