/*
This file is part of ProjectManager.

ProjectManager is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ProjectManager is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ProjectManager.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef QMLTABLEMODEL_H
#define QMLTABLEMODEL_H

#include <QSql>
#include <QSqlError>
#include <QDebug>
#include <QSqlRelationalTableModel>
#include <QSqlQuery>

template <class T> class QMLTableModel : public QSqlRelationalTableModel {

private:
    QHash<int, QByteArray> roles;

public:
    QMLTableModel(QObject *parent = Q_NULLPTR,
                  QSqlDatabase db = QSqlDatabase()) :
        QSqlRelationalTableModel(parent, db) {}

public:
    Q_INVOKABLE QVariant data(const QModelIndex &index, int role=Qt::DisplayRole ) const {
        if(index.row() >= rowCount()) {
            return QString("");
        }
        if(role < Qt::UserRole) {
            return QSqlRelationalTableModel::data(index, role);
        }
        else {
            // search for relationships
            for (int i = 0; i < columnCount(); i++) {
                if (this->relation(i).isValid()) {
                    return record(index.row()).value(QString(roles.value(role)));
                }
            }
            // if no valid relationship was found
            return QSqlRelationalTableModel::data(this->index(index.row(), role - Qt::UserRole - 1), Qt::DisplayRole);
        }
    }

    void generateRoleNames() {
        roles.clear();
        int nbCols = this->columnCount();

        for (int i = 0; i < nbCols; i++) {
            roles[Qt::UserRole + i + 1] = QVariant(this->headerData(i, Qt::Horizontal).toString()).toByteArray();
        }
    }

    virtual QHash<int, QByteArray> roleNames() const{ return roles; }

    virtual bool select() {
        auto result = QSqlRelationalTableModel::select();
        qDebug() << "Executed query: " << this->query().executedQuery();
        return result;
    }

protected:
    virtual T* _getRow(int i) {
        return new T(this, i);
    }

    virtual int _addRow() {
        int index = this->rowCount();
        if (this->insertRecord(index, this->record()) && this->select()) {
            return index;
        }
        qDebug() << this->lastError();
        return -1;
    }

    virtual void _removeRow(int i) {
        if (!this->removeRow(i))
            qDebug() << this->lastError();
        this->select();
    }
};

#endif // QMLTABLEMODEL_H
