/*
This file is part of ProjectManager.

ProjectManager is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ProjectManager is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ProjectManager.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QtSql>

#include "qmltablemodel.h"
#include "sortfilterproxymodel.h"
#include "project.h"
#include "item.h"

template <class T> T* buildTableModel(QString tableName, bool doSelect = true) {
    T* model = new T;
    model->setTable(tableName);
    model->generateRoleNames();
    model->setEditStrategy(QSqlTableModel::EditStrategy::OnFieldChange);

    if (doSelect && !model->select())
        throw model->database().lastError();
    return model;
}


QSqlError initDb(QQmlContext *rootContext) {
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("projectmanager.sqlite");
    if (!db.open())
        return db.lastError();

    auto projects = buildTableModel<ProjectModel>("Projects");
    rootContext->setContextProperty("projectsModel", projects);

    auto projectItems = buildTableModel<ItemModel>("Items", false);
    projectItems->setContainerType("Project");
    rootContext->setContextProperty("projectItemsModel", projectItems);

    return QSqlError();
}

int main(int argc, char *argv[])
{
#if defined(Q_OS_WIN)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    QSqlError error = initDb(engine.rootContext());
    if (error.isValid())
        engine.rootContext()->setContextProperty("sqlError", error.text());
    else
        engine.rootContext()->setContextProperty("sqlError", "");
    qmlRegisterType<SortFilterProxyModel>("org.qtproject.example", 1, 0, "SortFilterProxyModel");
    qmlRegisterInterface<Project>("Project");

    QLoggingCategory::setFilterRules(QStringLiteral("qt.qml.binding.removal.info=true"));

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
