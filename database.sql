CREATE TABLE IF NOT EXISTS Projects (
    ProjectId INTEGER PRIMARY KEY,
    ProjectName TEXT
);

CREATE TABLE IF NOT EXISTS Tasks (
    TaskId INTEGER PRIMARY KEY,
    ProjectId INTEGER REFERENCES Projects (ProjectId),
    TaskDescription TEXT,
    TaskNotes TEXT,
    TaskDeadlineDevelopmentStart DATE,
    TaskDeadlineDevelopmentEnd DATE,
    TaskDeadlineDSI DATE,
    TaskDeadlineUAT DATE,
    TaskDeadlinePROD DATE,
    TaskEstimate NUMBER,
    TaskActitimeProject
);

CREATE TABLE IF NOT EXISTS Items (
	ItemID INTEGER PRIMARY KEY,
	ContainerId INTEGER,
	ContainerType TEXT(100),
	ItemType TEXT(100),
	ItemContent TEXT,
	ItemOrder INTEGER
);