import QtQuick 2.8
import QtQuick.Controls 1.4
import org.qtproject.example 1.0
import QtQml 2.8 as Qml


Item {
    id: item1
    TableView {
        id: tableView
        anchors.top: parent.top
        anchors.topMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.right: parent.right
        anchors.rightMargin: 0
        height: 300

        frameVisible: false
        sortIndicatorVisible: true
        selectionMode: SelectionMode.SingleSelection

        TableViewColumn {
            id: projectNameColumn
            title: "Name"
            role: "ProjectName"
            movable: false
            resizable: false
            width: 100
        }

        model: projectsModel

        property variant selectedRow

        Connections {
            target: tableView.selection
            onSelectionChanged: {
//                console.log("selection changed");
                tableView.selection.forEach(function(rowIndex) {
                    tableView.selectedRow = projectsModel.getRow(rowIndex);
                });
                txtProjectName.focus = true;
            }

        }

    }

    ToolBar {
        id: toolBar
        height: 40
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.top: tableView.bottom
        anchors.topMargin: 0
        ToolButton {
            id: addButton
            text: "+"
            tooltip: "Add project"
            anchors.verticalCenter: parent.verticalCenter
            onClicked: {
                tableView.selection.clear();
                var i = projectsModel.addRow();
                if (i >= 0)
                    tableView.selection.select(i);
            }
        }

        ToolButton {
            id: deleteButton
            text: "-"
            tooltip: "Delete project"
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: addButton.right
            anchors.leftMargin: 5
            onClicked: {
                tableView.selection.forEach(function(rowIndex) {
                    projectsModel.removeRow(rowIndex);
                });
                tableView.selection.clear();
            }
        }
    }

    Item {
        id: projectEditForm
        anchors.topMargin: 10
        anchors.top: toolBar.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        Row {
            id: projectNameRow
            spacing: 0
            anchors.right: parent.right
            anchors.rightMargin: 0
            anchors.left: parent.left
            anchors.leftMargin: 0
            Label {
                id: lblProjectName
                text: "Project Name"
                width: 200
            }
            TextField {
                id: txtProjectName
                width: 500
                inputMask: qsTr("")
                placeholderText: "Project Name"
                text: tableView.selectedRow.ProjectName
                anchors.left: lblProjectName.right
                anchors.leftMargin: 0
                Qml.Binding {
                    target: tableView.selectedRow
                    property: "ProjectName"
                    value: txtProjectName.text
                    delayed: true
                }
            }
            Button {
                //anchors.right: projectEditForm.right
                //anchors.rightMargin: 10
                text: "+ Aggiungi sezione"
                anchors.right: parent.right
                anchors.rightMargin: 10
            }
        }

        Row {
            anchors.top: projectNameRow.bottom
            ListView {
                model: projectItemsModel
                Binding {
                    target: projectItemsModel
                    property: "containerId"
                    value: tableView.selectedRow.ProjectId
                }
                delegate: Text {
                    text: ItemContent
                }
            }
        }

    }

}
