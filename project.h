/*
This file is part of ProjectManager.

ProjectManager is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ProjectManager is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ProjectManager.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef PROJECT_H
#define PROJECT_H

#include "modelrowdto.h"

class Project : public ModelRowDto {
    typedef ModelRowDto super;
    Q_OBJECT
public:
    explicit Project(QSqlTableModel* model, int row) : super(model, row) {}

    DTO_COLUMN(ProjectId)
    DTO_COLUMN(ProjectName)
};

class ProjectModel : public QMLTableModel<Project> {
    Q_OBJECT
public:

    ProjectModel(QObject *parent = Q_NULLPTR, QSqlDatabase db = QSqlDatabase())
        : QMLTableModel<Project>(parent, db){}

    Q_INVOKABLE virtual Project* getRow(int i) { return _getRow(i); }
    Q_INVOKABLE int addRow() { return _addRow(); }
    Q_INVOKABLE void removeRow(int i) { _removeRow(i); }
};

#endif // PROJECT_H
