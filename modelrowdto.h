/*
This file is part of ProjectManager.

ProjectManager is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ProjectManager is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ProjectManager.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef MODELROWDTO_H
#define MODELROWDTO_H

#include <QObject>
#include <QVariant>
#include <QSqlRecord>
#include "qmltablemodel.h"

class ModelRowDto : public QObject {
    Q_OBJECT
public:
    explicit ModelRowDto(QSqlTableModel* model, int row);
protected:
    QVariant getProperty(QString name);
    QModelIndex getIndex(QString name);
    bool setProperty(QString name, QVariant value);
private:
    QSqlTableModel* model;
    int row;
};

#define DTO_COLUMN(name) \
    DTO_COLUMN_WITHNAME(name, #name)

#define DTO_COLUMN_WITHNAME(name, columnName) \
    Q_PROPERTY(QVariant name READ name WRITE set##name NOTIFY name##Changed) \
    QVariant name() { return getProperty(columnName); } \
    void set##name(QVariant value) { \
        if (setProperty(columnName, value)) \
        emit name##Changed(value);\
    } \
    Q_SIGNAL void name##Changed(QVariant value);


#endif // MODELROWDTO_H
